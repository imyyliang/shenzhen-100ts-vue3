export type ChannelItem = {
  id: number
  name: string
}
export type ChannelList = ChannelItem[]

interface Cover {
  type: 0 | 1 | 3;
  images?: string[];
}

export interface ArticleItem {
  art_id: string;
  title: string;
  aut_id: string;
  comm_count: number;
  pubdate: string;
  aut_name: string;
  is_top: number;
  cover: Cover;
}

export type ArticleList = ArticleItem[]




export interface Cover {
	type: number;
}

export interface Result {
	art_id: string;
	title: string;
	aut_id: string;
	comm_count: number;
	pubdate: string;
	aut_name: string;
	is_top: number;
	cover: Cover;
}

export interface Data {
	results: Result[];
	pre_timestamp: string;
}

export interface RootObject {
	data: Data;
	message: string;
}