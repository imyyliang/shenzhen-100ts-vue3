import axios from 'axios'

const request = axios.create({
  baseURL: 'http://geek.itheima.net', // 设置默认的 baseURL
  timeout: 5000, // 设置默认的 timeout
})
// 请求拦截器 这里没有用就不写
// 响应拦截器
request.interceptors.response.use(res=>{
  return res.data
}, err=>{
  return Promise.reject(err)
})

export default request