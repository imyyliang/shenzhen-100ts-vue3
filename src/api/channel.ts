import request from "../utils/request";

export const getChannelListAPI = () => {
  return request({
    url: '/v1_0/channels'
  })
}