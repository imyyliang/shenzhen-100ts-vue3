import request from "../utils/request";

export const getArticleAPI = (channel_id: number) => {
  return request({
    url: "/v1_0/articles",
    params: {
      channel_id,
      timestamp: Date.now,
    },
  });
};